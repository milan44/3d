package blob3d

import (
	"github.com/fogleman/pt/pt"
	"math"
	"math/rand"
	"time"
)

type MeshBuilder struct {
	Rand     Random
	CubeSize float64

	Cubes []pt.Cube
	mesh  *pt.Mesh
}

type Variation struct {
	Cube    pt.Cube
	Allowed bool
}

func NewRandomMeshBuilder() MeshBuilder {
	src := rand.NewSource(time.Now().UnixNano())
	rnd := rand.New(src)

	return NewMeshBuilder(rnd, 1)
}

func NewMeshBuilder(rnd *rand.Rand, cubeSize float64) MeshBuilder {
	return MeshBuilder{
		Rand:     Random{rnd: rnd},
		CubeSize: cubeSize,
		Cubes:    make([]pt.Cube, 0),
		mesh:     pt.NewMesh(nil),
	}
}

func (m MeshBuilder) Mesh() pt.Mesh {
	return *m.mesh
}

func (m *MeshBuilder) AddRandomCube() {
	defer func() {
		recover()
	}()

	if len(m.Cubes) == 0 {
		m.AddCube(m.SimpleCube(pt.V(0, 0, 0)))
	} else {
		last := m.Cubes[len(m.Cubes)-1]
		m.AddCube(m.RandomCube(last))
	}
}

func (m *MeshBuilder) AddCube(c pt.Cube) {
	m.Cubes = append(m.Cubes, c)
	m.mesh.Add(c.Mesh())
}

func (m MeshBuilder) SimpleCube(min pt.Vector) pt.Cube {
	max := m.SizeVector(min)

	return *pt.NewCube(min, max, pt.DiffuseMaterial(pt.HexColor(0xffffff)))
}

func (m MeshBuilder) RandomCube(c pt.Cube) pt.Cube {
	center := c.BoundingBox().Center()

	min := m.CubeSize / 4
	max := m.CubeSize - min

	move := pt.V(
		m.RandomAbs(m.Rand.Float64(min, max)),
		m.RandomAbs(m.Rand.Float64(min, max)),
		m.RandomAbs(m.Rand.Float64(min, max)),
	)

	center = center.Add(move)
	half := m.CubeSize / 2

	minV := pt.V(
		center.X-half,
		center.Y-half,
		center.Z-half,
	)
	maxV := m.SizeVector(minV)

	cube := pt.NewCube(minV, maxV, pt.DiffuseMaterial(pt.HexColor(0xffffff)))

	return *cube
}

func (m MeshBuilder) RandomAbs(x float64) float64 {
	if m.Rand.Float64(0, 1) <= 0.5 {
		return -x
	}
	return x
}

func Distance(c1, c2 pt.Cube) float64 {
	b1 := c1.BoundingBox().Center()
	b2 := c2.BoundingBox().Center()

	return math.Sqrt(
		math.Pow(b1.X-b2.X, 2) +
			math.Pow(b1.Y-b2.Y, 2) +
			math.Pow(b1.Z-b2.Z, 2),
	)
}
