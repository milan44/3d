package blob3d

import (
	"math/rand"
	"testing"
	"time"
)

func TestCreateRandomBlob(t *testing.T) {
	src := rand.NewSource(time.Now().UnixNano())
	rnd := rand.New(src)

	err := CreateRandomBlob(rnd, false, "out.png", nil)
	if err != nil {
		panic(err)
	}
}
