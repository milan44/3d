package blob3d

import (
	"github.com/lucasb-eyer/go-colorful"
)

func randomPimp(rnd Random) colorful.Color {
	return colorful.Hcl(rnd.Float64(0, 1)*360.0, 0.5+rnd.Float64(0, 1)*0.3, 0.5+rnd.Float64(0, 1)*0.3)
}
