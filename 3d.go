package blob3d

import (
	"github.com/fogleman/pt/pt"
	"image/png"
	"os"
)

type RenderConfig struct {
	SamplesPerPixel int
	AdaptiveSamples int
	MaxBounces      int
}

func NewScene() pt.Scene {
	scene := pt.Scene{}
	scene.Color = pt.White

	return scene
}

func RenderScene(scene pt.Scene, path string, m MeshBuilder, cfg RenderConfig) error {
	f, err := os.OpenFile(path, os.O_CREATE|os.O_RDWR, 0777)
	if err != nil {
		return err
	}

	scene.Add(pt.NewSphere(pt.V(4, 10, 1), 1, pt.LightMaterial(pt.White, 80)))

	scene.Add(pt.NewCube(pt.V(-30, -100, -30), pt.V(30, -1, 30), m.Rand.DiffuseColor()))

	camera := pt.LookAt(pt.V(2, 3, 4), pt.V(0, 0, 0), pt.V(0, 1, 0), 40)
	sampler := pt.NewSampler(2, cfg.MaxBounces)

	renderer := pt.NewRenderer(&scene, &camera, sampler, 256, 256)
	renderer.AdaptiveSamples = cfg.AdaptiveSamples
	renderer.SamplesPerPixel = cfg.SamplesPerPixel
	renderer.Verbose = false

	img := renderer.Render()

	return png.Encode(f, img)
}
