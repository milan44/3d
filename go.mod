module gitlab.com/milan44/3d

go 1.15

require (
	github.com/fogleman/pt v0.0.0-20170619012416-6fa0015c2178
	github.com/lucasb-eyer/go-colorful v1.2.0
)
