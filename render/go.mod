module render

go 1.15

require (
	github.com/dustin/go-humanize v1.0.0
	github.com/fogleman/pt v0.0.0-20170619012416-6fa0015c2178
	github.com/lucasb-eyer/go-colorful v1.2.0
	github.com/skratchdot/open-golang v0.0.0-20200116055534-eef842397966
	gitlab.com/milan44/3d v1.0.3
	gitlab.com/milan44/seed v0.0.0-20210404134730-0ca6748d210c
)
