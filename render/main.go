package main

import (
	"flag"
	"fmt"
	"github.com/dustin/go-humanize"
	"github.com/fogleman/pt/pt"
	"github.com/lucasb-eyer/go-colorful"
	"github.com/skratchdot/open-golang/open"
	blob3d "gitlab.com/milan44/3d"
	"gitlab.com/milan44/seed"
	"image/png"
	"math/rand"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

func main() {
	rand.Seed(time.Now().UnixNano())

	stl := flag.String("i", "", "Input file")
	out := flag.String("o", "", "Output file (png)")
	size := flag.Int("s", 512, "Output Size (-s 512 -> 512px x 512px)")
	create := flag.String("c", "", "If a random mesh should be generated (using this as the seed)")
	random := flag.Bool("r", false, "Will set -c to a random seed")
	fullSize := flag.Bool("f", false, "Set -s to 2560, -spp to 1028 and -ads to 256")

	spp := flag.Int("spp", 50, "Samples per pixel")
	ads := flag.Int("ads", 128, "Adaptive Samples")
	fov := flag.Int("fov", 50, "Field of View")

	flag.Parse()

	output := strings.TrimSpace(*out)

	if *random {
		*create = randomSeed()
	}

	if *fullSize {
		*spp = 128
		*ads = 256
		*size = 2560
	}

	if strings.TrimSpace(*stl) == "" && strings.TrimSpace(*create) == "" {
		fmt.Println("Either the -i or -c flag have to be set")
		return
	}

	if *spp < 1 {
		fmt.Println("-spp can't be smaller than 1")
		return
	} else if *ads < 0 {
		fmt.Println("-ads can't be smaller than 0")
		return
	} else if *fov < 10 {
		fmt.Println("-fov can't be smaller than 10")
		return
	} else if *size < 10 {
		fmt.Println("-s can't be smaller than 10")
		return
	}

	scene := pt.Scene{}
	scene.Color = pt.White

	src := rand.NewSource(time.Now().UnixNano())

	var (
		mesh *pt.Mesh
		err  error
		rnd  = rand.New(src)
	)

	fmt.Println("[config]")
	fmt.Printf("    Samples     -> %d\n", *spp)
	fmt.Printf("    Adaptive S. -> %d\n", *ads)
	fmt.Printf("    Field of V. -> %d\n", *fov)

	if strings.TrimSpace(*create) != "" {
		rnd = seed.Rand(*create)
		fmt.Printf("    Input Seed  -> %s\n", *create)

		hex := seed.Parse(*create).Format(16)
		if output == "" {
			output = "./blob_" + hex + ".png"
		}

		m := blob3d.NewMeshBuilder(rnd, 1)

		amount := m.Rand.Int(5000) + 1000
		for i := 0; i < amount; i++ {
			m.AddCube()
		}

		msh := m.Mesh()
		msh.SetMaterial(material(rnd))
		mesh = &msh
	} else if strings.HasSuffix(*stl, ".stl") {
		fmt.Printf("    Input File  -> %s\n", *stl)

		mesh, err = pt.LoadSTL(*stl, material(rnd))
		if err != nil {
			fmt.Println("Invalid input file: " + err.Error())
			return
		}
	} else if strings.HasSuffix(*stl, ".obj") {
		fmt.Printf("    Input File  -> %s\n", *stl)

		mesh, err = pt.LoadOBJ(*stl, material(rnd))
		if err != nil {
			fmt.Println("Invalid input file: " + err.Error())
			return
		}
	} else {
		fmt.Println("Invalid input file (Neither .obj nor .stl)")
		return
	}

	if output == "" {
		fmt.Println("The -o flag has to be set")
	}

	fmt.Printf("    Triangles   -> %s\n", humanize.Comma(int64(len(mesh.Triangles))))
	fmt.Printf("    Output File -> %s\n", filepath.Base(output))
	fmt.Println("")

	box := pt.Box{
		Min: pt.V(-1, -0.8, -1),
		Max: pt.V(1.6, 1.6, 1.3),
	}
	mesh.FitInside(box, box.Center())

	scene.Add(mesh)

	f, err := os.OpenFile(output, os.O_CREATE|os.O_RDWR, 0777)
	if err != nil {
		fmt.Println("Failed to create output file: " + err.Error())
		return
	}

	scene.Add(pt.NewSphere(pt.V(4, 10, 1), 1, pt.LightMaterial(pt.White, 80)))

	scene.Add(pt.NewCube(pt.V(-30, -100, -30), pt.V(30, -1, 30), material(rnd)))

	camera := pt.LookAt(pt.V(2, 3, 4), pt.V(0, 0, 0), pt.V(0, 1, 0), float64(*fov))
	sampler := pt.NewSampler(2, 2)

	renderer := pt.NewRenderer(&scene, &camera, sampler, *size, *size)
	renderer.AdaptiveSamples = *ads
	renderer.SamplesPerPixel = *spp

	img := renderer.Render()

	err = png.Encode(f, img)
	if err != nil {
		fmt.Println("Failed to encode png: " + err.Error())
		return
	}

	_ = open.Run(f.Name())
	fmt.Println("Rendered preview to " + f.Name())
}

func randomSeed() string {
	runes := []rune("abcdefghijklmnopqrstuvwxyz")
	out := ""
	length := rand.Intn(5) + 10
	for i := 0; i < length; i++ {
		out += string(runes[rand.Intn(len(runes))])
	}
	return out
}

func material(rnd *rand.Rand) pt.Material {
	c := color(rnd)
	for !c.IsValid() {
		c = color(rnd)
	}

	i, _ := strconv.ParseInt(c.Hex()[1:], 16, 64)

	return pt.DiffuseMaterial(pt.HexColor(int(i)))
}

func color(rnd *rand.Rand) colorful.Color {
	return colorful.Hcl(rnd.Float64()*360.0, 0.5+rnd.Float64()*0.3, 0.5+rnd.Float64()*0.3)
}
