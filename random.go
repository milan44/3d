package blob3d

import (
	"github.com/fogleman/pt/pt"
	"math/rand"
	"strconv"
)

type Random struct {
	rnd *rand.Rand
}

func (r Random) Float64(min, max float64) float64 {
	return min + r.rnd.Float64()*(max-min)
}

func (r Random) Int(max int) int {
	if max == 0 {
		panic("max cannot be 0")
	}

	return r.rnd.Intn(max)
}

func (r Random) DiffuseColor() pt.Material {
	c := randomPimp(r)
	for !c.IsValid() {
		c = randomPimp(r)
	}

	i, _ := strconv.ParseInt(c.Hex()[1:], 16, 64)

	return pt.DiffuseMaterial(pt.HexColor(int(i)))
}

func (m MeshBuilder) SizeVector(min pt.Vector) pt.Vector {
	return pt.V(
		min.X+m.CubeSize,
		min.Y+m.CubeSize,
		min.Z+m.CubeSize,
	)
}
