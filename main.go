package blob3d

import (
	"github.com/fogleman/pt/pt"
	"math/rand"
	"time"
)

func CreateRandomBlob(rnd *rand.Rand, stlOnly bool, output string, cfg *RenderConfig) error {
	m := NewMeshBuilder(rnd, 1)

	amount := m.Rand.Int(5000) + 1000
	for i := 0; i < amount; i++ {
		m.AddRandomCube()
	}

	mesh := m.Mesh()

	return RenderMesh(mesh, stlOnly, output, cfg, m)
}

func RenderSimpleMesh(mesh pt.Mesh, output string, cfg *RenderConfig) error {
	src := rand.NewSource(time.Now().UnixNano())
	rnd := rand.New(src)

	m := NewMeshBuilder(rnd, 1)

	return RenderMesh(mesh, false, output, cfg, m)
}

func RenderMesh(mesh pt.Mesh, stlOnly bool, output string, cfg *RenderConfig, m MeshBuilder) error {
	if cfg == nil {
		cfg = &RenderConfig{
			SamplesPerPixel: 15,
			AdaptiveSamples: 50,
			MaxBounces:      4,
		}
	}

	scene := NewScene()

	mesh.SetMaterial(m.Rand.DiffuseColor())
	box := pt.Box{
		Min: pt.V(-1, -0.8, -1),
		Max: pt.V(1.6, 1.6, 1.3),
	}
	mesh.FitInside(box, box.Center())

	scene.Add(&mesh)

	if stlOnly {
		return mesh.SaveSTL(output)
	}

	return RenderScene(scene, output, m, *cfg)
}
