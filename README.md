### .stl and .obj CLI Renderer

**Usage**
```
render -i cool.stl -o image.png -s 1000
```
Will render the file `cool.stl` to the output file `image.png` which will be 1000px by 1000px big.

When the -c flag is provided (any string) it will generate a random mesh based on the value of the -c flag.

**Downloads**  
[Windows](render/blob.exe) - [Linux](render/blob)
